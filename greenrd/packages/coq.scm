;;; Copyright © 2020 Robin Greern <greenrd@greenrd.org>

;;; This file is NOT part of GNU Guix, but it extends it.

;;; This channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (greenrd packages coq)
  #:use-module (gnu packages coq)
  #:use-module (gnu packages ocaml)
  #:use-module (guix packages))

(define-public coq-bytecode
  (package
    (inherit coq)
    (propagated-inputs
     `(("ocaml-num" ,ocaml-num)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'make-git-checkout-writable
           (lambda _
             (for-each make-file-writable (find-files "."))
             #t))
         (add-after 'unpack 'disable-coqide-byte-install
                    (lambda _
                      (substitute* "Makefile.install"
                        ((" install-coqide-byte") ""))
                      #t))
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (libdir (string-append out "/lib/ocaml/site-lib/coq"))
                    (mandir (string-append out "/share/man"))
                    (browser "icecat -remote \"OpenURL(%s,new-tab)\""))
               (invoke "./configure"
                       "-prefix" out
                       "-libdir" libdir
                       "-mandir" mandir
                       "-browser" browser
                       "-coqide" "opt"))))
         (replace 'build
           (lambda _
             (invoke "make"
                     "-j" (number->string (parallel-job-count))
                     "byte")))
         (delete 'check)
         (add-after 'install 'remove-duplicate
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (coqtop (string-append bin "/coqtop"))
                    (coqidetop (string-append bin "/coqidetop"))
                    (coqtop.byte (string-append coqtop ".byte"))
                    (coqidetop.byte (string-append coqidetop ".byte")))
               ;; These files are exact copies without `.byte` extension.
               ;; Removing these saves 35 MiB in the resulting package.
               ;; Unfortunately, completely deleting them breaks coqide.
               (delete-file coqtop.byte)
               (delete-file coqidetop.byte)
               (symlink coqtop coqtop.byte)
               (symlink coqidetop coqidetop.byte))
             #t))
         (add-after 'install 'install-ide
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (ide (assoc-ref outputs "ide")))
               (mkdir-p (string-append ide "/bin"))
               (rename-file (string-append out "/bin/coqide")
                            (string-append ide "/bin/coqide")))
             #t))
         (add-after 'install 'install-top
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (copy-file "topbin/coqtop_bin.ml" (string-append bytecode "/lib/ocaml/site-lib/coq/toplevel/coqtop_bin.ml")))
             #t))
         (add-after 'install-top 'check
           (lambda _
             (with-directory-excursion "test-suite"
               ;; Fails because the output is not formatted as expected.
               (delete-file-recursively "coq-makefile/timing")
               (invoke "make"))))
         )))))
